package main

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"

	//"io/ioutil"
	"os"

	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	//"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mail.v2"
	"gorm.io/datatypes"
	// "gorm.io/driver/sqlite"
	// "gorm.io/gorm"
	// "log"
)

// type UserWithJSON struct {
// 	gorm.Model
// 	Name       string
// 	Attributes datatypes.JSON
// }

// package level variable
var dbName = "nurse_db"
var db *gorm.DB

var nures_collection = "new_nurses_collection"

var nurse_logs_collection = "nurse_logs_collection"

// var settings_collection = "settings_collection"
var settings_collection = "new_settings_collection"

var tickets_collection = "tickets_collection"

var secretKey = []byte("secret-key")
var users_collection = "users_collection"

// var FacilityCollection = "Fac"
// var DriverCollection = "Drivers"
// var VehicleCollection = "Vehicles"
// var BrokerCollection = "Brokers"

var mongoURI = "mongodb+srv://dandisolutions09:GAFmLYi25DojAuM1@tms-cluster.cp6qfmq.mongodb.net/"

//var mongoURI = "mongodb+srv://oluwashayomi:eniola1990@cluster0.kcef1.mongodb.net/"

var client *mongo.Client

type UserWithJSON struct {
	gorm.Model
	Name       string         `json:"name"`
	Attributes datatypes.JSON `json:"attributes"`
}

type NurseData struct {
	gorm.Model
	Name               string         `json:"name"`
	Department         string         `json:"department"`
	Wards              datatypes.JSON `json:"wards"`
	Grade              string         `json:"grade"`
	SignatureImageData string         `json:"signatureImageData"`
}

func SendMail(subject, body, description string) (int, error) {
	// Specify the path to the .txt file
	filePath := "currentTicketNumber.txt"

	// Read the file contents
	content, err := os.ReadFile(filePath)
	if err != nil {
		fmt.Println("Error reading file:", err)
		//return
	}

	// Print the file contents as a string
	fmt.Println("File Contents:")
	fmt.Println(string(content))
	newContent := string(content)

	//int_ticketNumber := int(newContent)

	ticketnumber, err := strconv.Atoi(newContent)
	if err != nil {
		fmt.Println("Error converting string to integer:", err)
		//return
	}

	fmt.Println("number", ticketnumber+1)
	new_ticket := ticketnumber + 1

	// Open the file for writing (create if not exists, truncate otherwise)
	file, err := os.Create(filePath)
	if err != nil {
		fmt.Println("Error creating file:", err)
		//return
	}
	defer file.Close()

	_, err = file.WriteString(fmt.Sprint(new_ticket))
	if err != nil {
		fmt.Println("Error writing to file:", err)
		//return
	}

	// Sender's email credentials
	from := "collinsdon09@gmail.com"
	// It's better not to hardcode the password; use environment variable or configuration file
	password := "zuhn wzwz vkef eglw"

	// Recipient email address
	to := "dandisolutions09@gmail.com"

	// SMTP server configuration
	smtpHost := "smtp.gmail.com"
	smtpPort := 587

	// Create a new email message
	m := mail.NewMessage()
	m.SetHeader("From", from)
	m.SetHeader("To", to)
	m.SetHeader("Subject", "Ticket#"+fmt.Sprint(new_ticket)+":-"+subject)
	m.SetBody("text/plain", description)

	// Set up the SMTP client
	d := mail.NewDialer(smtpHost, smtpPort, from, password)

	// Send the email
	err = d.DialAndSend(m)
	if err != nil {
		fmt.Println("There was an error sending email.")
		panic(err)
	}

	fmt.Println("Email sent successfully.")

	return new_ticket, nil
}

func init() {

	var err error

	// Set up MongoDB client
	clientOptions := options.Client().ApplyURI(mongoURI)

	client, err = mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		fmt.Println("Error connecting to MongoDB:", err)
		return
	}

	// Check the connection
	err = client.Ping(context.Background(), nil)
	if err != nil {
		fmt.Println("Error pinging MongoDB:", err)
		return
	}

	fmt.Println("Connected to MongoDB")
}

func createNurse(w http.ResponseWriter, r *http.Request) {
	var newUser NurseData
	err := json.NewDecoder(r.Body).Decode(&newUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	result := db.Create(&newUser)
	if result.Error != nil {
		http.Error(w, result.Error.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newUser)
}

// GetAllNurseData retrieves all nurse data from the database
func GetAllNurseDataHandler(w http.ResponseWriter, r *http.Request) {
	var nurses []NurseData
	result := db.Find(&nurses)
	if result.Error != nil {
		http.Error(w, result.Error.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(nurses)
}

// UpdateNurseDataHandler handles the PUT request to update a specific nurse by ID
func UpdateNurseDataHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var nurse NurseData
	result := db.First(&nurse, params["id"])
	if result.Error != nil {
		http.Error(w, result.Error.Error(), http.StatusNotFound)
		return
	}

	var updatedNurse NurseData
	err := json.NewDecoder(r.Body).Decode(&updatedNurse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	result = db.Model(&nurse).Updates(updatedNurse)
	if result.Error != nil {
		http.Error(w, result.Error.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(nurse)
}

// DeleteNurseDataHandler handles the DELETE request to delete a specific nurse by ID
func DeleteNurseDataHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var nurse NurseData
	result := db.Delete(&nurse, params["id"])
	if result.Error != nil {
		fmt.Println("delete unsuccessfully!")
		http.Error(w, result.Error.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// Define a struct for your data (adjust fields as needed)
func main() {
	// Create a new Gorilla Mux router

	// var err error
	// db, err = gorm.Open(sqlite.Open("mydatabase.db"), &gorm.Config{})
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// // AutoMigrate will create the table based on the User struct
	// db.AutoMigrate(&UserS{})

	//SendMail()

	router := mux.NewRouter()

	// Use the handlers.CORS middleware to handle CORS
	corsHandler := handlers.CORS(
		handlers.AllowedHeaders([]string{"Content-Type", "Authorization"}),
		handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"}),
		handlers.AllowedOrigins([]string{"*"}),
	)

	// Attach the CORS middleware to your router

	//FACILITY ROUTES
	router.HandleFunc("/add-nurse", handleAddNurse).Methods("POST")
	router.HandleFunc("/get-nurses", handleGetNurses).Methods("GET")
	router.HandleFunc("/get-active-nurses", handleGetActiveNurses).Methods("GET")

	router.HandleFunc("/nurse/{name}", handleGetNurseByName).Methods("GET")
	router.HandleFunc("/edit-nurse/{id}", handleUpdateNurse).Methods("PUT")
	router.HandleFunc("/delete-nurse/{id}", handleDeleteNurse).Methods("DELETE")

	router.HandleFunc("/deactivate-nurse/{id}", handleDeactivateNurse).Methods("PUT")
	router.HandleFunc("/activate-nurse/{id}", handleActivateNurse).Methods("PUT")

	router.HandleFunc("/deactivate-user/{id}", handleDeactivateUser).Methods("PUT")
	router.HandleFunc("/activate-user/{id}", handleActivateUser).Methods("PUT")

	//GET BY ID
	//LOGIN ROUTES
	router.HandleFunc("/login", LoginHandler).Methods("POST")
	// router.HandleFunc("/protected", ProtectedHandler).Methods("GET")
	router.HandleFunc("/get-username/{username}", handleGetUserByName).Methods("GET")
	router.HandleFunc("/register", RegisterUser).Methods("POST")
	router.HandleFunc("/get-users", handleGetUsers).Methods("GET")
	router.HandleFunc("/update-user/{id}", handleUpdateUser).Methods("PUT")
	router.HandleFunc("/reset-password/{id}", handleResetPassword).Methods("PUT")

	router.HandleFunc("/encrypt", encryption).Methods("GET")
	router.HandleFunc("/create-new-settings", handlecreateSettings).Methods("POST")
	router.HandleFunc("/update-settings/{id}", handleUpdateSetting).Methods("PUT")
	router.HandleFunc("/get-settings", handleGetSettings).Methods("GET")

	// http://localhost:8080/update-user/

	//LOGS ENDPOINTS
	router.HandleFunc("/create-log", handlecreateNurseLog).Methods("POST")
	router.HandleFunc("/get-logs", handleGetNurseLogs).Methods("GET")

	//SQLITE generation
	//	router.HandleFunc("/users", createUser).Methods("POST")
	router.HandleFunc("/create", createNurse).Methods("POST")
	router.HandleFunc("/nurses", GetAllNurseDataHandler).Methods("GET")
	router.HandleFunc("/update-nurse/{id}", UpdateNurseDataHandler).Methods("PUT")
	router.HandleFunc("/remove-nurse/{id}", DeleteNurseDataHandler).Methods("DELETE")

	//TICKETING
	router.HandleFunc("/add-ticket", handleAddTicket).Methods("POST")
	router.HandleFunc("/tickets", handleGetTickets).Methods("GET")

	// Start the server on port 8080
	http.Handle("/", corsHandler(router))
	fmt.Println("Server listening on :8080")
	http.ListenAndServe(":8080", nil)
}
